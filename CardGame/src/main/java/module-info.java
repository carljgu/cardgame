module edu.ntnu.idatt2001.carljgu.cardgame {
  requires javafx.controls;
  requires javafx.fxml;
  requires javafx.graphics;

  opens edu.ntnu.idatt2001.carljgu.cardgame to javafx.fxml;
  exports edu.ntnu.idatt2001.carljgu.cardgame ;
}
