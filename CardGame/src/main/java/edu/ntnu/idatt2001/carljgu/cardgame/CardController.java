package edu.ntnu.idatt2001.carljgu.cardgame;

import edu.ntnu.idatt2001.carljgu.cardgame.cards.DeckOfCards;
import edu.ntnu.idatt2001.carljgu.cardgame.cards.HandOfCards;
import edu.ntnu.idatt2001.carljgu.cardgame.cards.PlayingCard;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.text.Text;

public class CardController {

  @FXML
  private Text cardsonhand;

  @FXML
  private Text flush;

  @FXML
  private Text hearts;

  @FXML
  private Text queenofspade;

  @FXML
  private Text sum;

  DeckOfCards deck;
  HandOfCards hand;

  @FXML
  public void onCheckHand(ActionEvent event) {
    if (hand == null) {
      cardsonhand.setText("No cards on hand. Click deal hand");
      return;
    }

    int sumOfAllFaces = hand.sumOfCards();
    ArrayList<PlayingCard> cards = hand.listOfSuit('H');
    boolean isFlush = hand.isFlush();
    boolean hasCard = hand.isCardInHand(new PlayingCard('S', 12));

    sum.setText(String.valueOf(sumOfAllFaces));

    if (cards.isEmpty()) hearts.setText(
      "No cards with hearts"
    ); else hearts.setText(String.valueOf(cards));

    if (isFlush) flush.setText("Yes"); else flush.setText("No");

    if (hasCard) queenofspade.setText("Yes"); else queenofspade.setText("No");
  }

  @FXML
  public void onDealHand(ActionEvent event) {
    deck = new DeckOfCards();
    hand = deck.dealHand(5);
    cardsonhand.setText(hand.toString());

    sum.setText("-");
    hearts.setText("-");
    flush.setText("-");
    queenofspade.setText("-");
  }
}
