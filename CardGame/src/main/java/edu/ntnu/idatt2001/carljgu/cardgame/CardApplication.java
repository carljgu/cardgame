package edu.ntnu.idatt2001.carljgu.cardgame;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class CardApplication extends Application {

  @Override
  public void start(Stage stage) throws IOException {
    FXMLLoader fxmlLoader = new FXMLLoader(
      CardApplication.class.getClassLoader().getResource("hand.fxml")
    );
    Scene scene = new Scene(fxmlLoader.load(), 720, 480);
    stage.setTitle("Cards analysis");

    Image icon = new Image("DHPS.png");
    stage.getIcons().add(icon);

    stage.setScene(scene);
    stage.show();
  }

  public static void main(String[] args) {
    launch();
  }
}
