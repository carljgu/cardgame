package edu.ntnu.idatt2001.carljgu.cardgame.cards;

import java.util.ArrayList;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * The type Hand of cards.
 *
 * @author Carl Gützkow
 * @version 1.04.2022
 */
public class HandOfCards {

  private final ArrayList<PlayingCard> hand;

  /**
   * Instantiates a new Hand of cards.
   */
  public HandOfCards() {
    this.hand = new ArrayList<>();
  }

  /**
   * Instantiates a new Hand of cards.
   *
   * @param hand the hand
   */
  public HandOfCards(ArrayList<PlayingCard> hand) {
    this.hand = hand;
  }

  /**
   * Gets hand.
   *
   * @return the hand
   */
  public ArrayList<PlayingCard> getHand() {
    return hand;
  }

  /**
   * Add card boolean.
   *
   * @param card the card
   * @return the boolean
   */
  public boolean addCard(PlayingCard card) {
    if (hand.contains(card)) return false;
    return hand.add(card);
  }

  /**
   * Is card in hand boolean.
   *
   * @param card the card
   * @return the boolean
   */
  public boolean isCardInHand(PlayingCard card) {
    return hand.stream().anyMatch(s -> s.equals(card));
  }

  /**
   * Sum of cards int.
   *
   * @return the int
   */
  public int sumOfCards() {
    return hand.stream().map(PlayingCard::getFace).reduce(0, Integer::sum);
  }

  /**
   * List of suit array list.
   *
   * @param suit the suit
   * @return the array list
   */
  public ArrayList<PlayingCard> listOfSuit(char suit) {
    return hand
      .stream()
      .filter(n -> n.getSuit() == suit)
      .collect(Collectors.toCollection(ArrayList::new));
  }

  /**
   * List of face array list.
   *
   * @param face the face
   * @return the array list
   */
  public ArrayList<PlayingCard> listOfFace(int face) {
    return hand
      .stream()
      .filter(n -> n.getFace() == face)
      .collect(Collectors.toCollection(ArrayList::new));
  }

  /**
   * Is flush boolean.
   *
   * @return true if flush
   */
  public boolean isFlush() {
    return hand.stream()
            .map(PlayingCard::getSuit)
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
            .values().stream().anyMatch(n -> n >= 5);

  }

  @Override
  public String toString() {
    return hand.toString().replace('[', ' ').replace(']', ' ');
  }
}
