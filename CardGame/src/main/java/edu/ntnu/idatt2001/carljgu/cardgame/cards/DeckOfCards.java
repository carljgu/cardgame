package edu.ntnu.idatt2001.carljgu.cardgame.cards;

import java.util.ArrayList;
import java.util.Random;
import java.util.function.Function;

/**
 * The type Deck of cards.
 *
 * @author Carl Gützkow
 * @version 13.03.2022
 */
public class DeckOfCards {

  private ArrayList<PlayingCard> deck;
  private final char[] suits = { 'S', 'H', 'D', 'C' };

  /**
   * Instantiates a new Deck of cards.
   */
  public DeckOfCards() {
    deck = new ArrayList<>();
    for (int i = 0; i < suits.length; i++) {
      for (int j = 1; j <= 13; j++) {
        deck.add(new PlayingCard(suits[i], j));
      }
    }
  }

  /**
   * Deal hand hand of cards.
   *
   * @param amount the amount
   * @return the hand of cards
   */
  public HandOfCards dealHand(int amount) {
    if (getDeckSize() == 0) throw new IllegalArgumentException("Deck is empty");
    if (amount < 0) throw new IllegalArgumentException(
      "Amount of cards drawn can not be negative"
    );
    if (amount > getDeckSize()) throw new IllegalArgumentException(
      "Amount of cards can not be more than the size of the deck"
    );
    Random random = new Random();

    Function<Integer, HandOfCards> assign = n -> {
      HandOfCards cards = new HandOfCards();
      for (int i = 0; i < n; i++) {
        cards.addCard(deck.remove(random.nextInt(deck.size())));
      }
      return cards;
    };
    return assign.apply(amount);
  }

  /**
   * Gets deck size.
   *
   * @return the deck size
   */
  public int getDeckSize() {
    return deck.size();
  }
}
