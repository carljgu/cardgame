package edu.ntnu.idatt2001.carljgu.cardgame;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.carljgu.cardgame.cards.DeckOfCards;
import edu.ntnu.idatt2001.carljgu.cardgame.cards.HandOfCards;
import edu.ntnu.idatt2001.carljgu.cardgame.cards.PlayingCard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class HandOfCardFlushTest {

  @Test
  public void hand_is_flush_if_all_five_cards_are_spades() {
    HandOfCards hand = new HandOfCards();
    for (int i = 1; i <= 5; i++) hand.addCard(new PlayingCard('S', i));

    assertTrue(hand.isFlush());
  }

  @Test
  public void hand_is_not_flush_if_all_five_cards_are_spades_except_one() {
    HandOfCards hand = new HandOfCards();
    for (int i = 1; i <= 4; i++) hand.addCard(new PlayingCard('S', i));
    hand.addCard(new PlayingCard('H', 5));

    assertFalse(hand.isFlush());
  }

  @Test
  public void hand_is_not_flush_if_all_cards_are_spades_but_with_four_cards() {
    HandOfCards hand = new HandOfCards();
    for (int i = 1; i <= 4; i++) hand.addCard(new PlayingCard('S', i));

    assertFalse(hand.isFlush());
  }

  @Test
  public void hand_is_flush_if_all_cards_are_spades_but_with_six_cards() {
    HandOfCards hand = new HandOfCards();
    for (int i = 1; i <= 6; i++) hand.addCard(new PlayingCard('S', i));

    assertTrue(hand.isFlush());
  }
}
