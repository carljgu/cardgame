package edu.ntnu.idatt2001.carljgu.cardgame;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import edu.ntnu.idatt2001.carljgu.cardgame.cards.DeckOfCards;
import edu.ntnu.idatt2001.carljgu.cardgame.cards.HandOfCards;
import edu.ntnu.idatt2001.carljgu.cardgame.cards.PlayingCard;
import org.junit.jupiter.api.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class DeckOfCardsTest {

  DeckOfCards deck;

  @BeforeEach
  public void instantiateDeck() {
    deck = new DeckOfCards();
  }

  @Test
  public void new_deck_of_cards_has_fifty_two_cards() {
    assertEquals(52, deck.getDeckSize());
  }

  @Test
  public void non_empty_hand_of_cards_has_n_cards_after_dealt_n_cards() {
    HandOfCards hand = new HandOfCards();
    hand.addCard(new PlayingCard('H', 1));

    hand = deck.dealHand(52);

    assertEquals(52, hand.getHand().size());
  }

  @Nested
  public class Deal_hand_throws_exception {

    @Test
    public void amount_of_cards_drawn_can_not_be_negative() {
      try {
        deck.dealHand(-1);
        fail();
      } catch (IllegalArgumentException exception) {
        assertEquals(
          "Amount of cards drawn can not be negative",
          exception.getMessage()
        );
      }
    }

    @Test
    public void amount_of_cards_drawn_can_not_be_more_than_cards_in_deck() {
      try {
        deck.dealHand(53);
        fail();
      } catch (IllegalArgumentException exception) {
        assertEquals(
          "Amount of cards can not be more than the size of the deck",
          exception.getMessage()
        );
      }
    }

    @Test
    public void deck_can_not_be_empty_when_drawing_cards() {
      deck.dealHand(52);
      try {
        deck.dealHand(1);
        fail();
      } catch (IllegalArgumentException exception) {
        assertEquals("Deck is empty", exception.getMessage());
      }
    }
  }
}
