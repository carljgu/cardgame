package edu.ntnu.idatt2001.carljgu.cardgame;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.carljgu.cardgame.cards.HandOfCards;
import edu.ntnu.idatt2001.carljgu.cardgame.cards.PlayingCard;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class HandOfCardTest {

  @Test
  public void sum_of_four_aces_is_four() {
    HandOfCards hand = new HandOfCards();
    hand.addCard(new PlayingCard('S', 1));
    hand.addCard(new PlayingCard('H', 1));
    hand.addCard(new PlayingCard('D', 1));
    hand.addCard(new PlayingCard('C', 1));

    assertEquals(4, hand.sumOfCards());
  }

  @Test
  public void queen_of_spades_exists_in_hand_without_equal_reference_point() {
    HandOfCards hand = new HandOfCards();
    PlayingCard card = new PlayingCard('S', 12);
    hand.addCard(card);
    card = new PlayingCard('S', 12);

    assertTrue(hand.isCardInHand(card));
  }
}
