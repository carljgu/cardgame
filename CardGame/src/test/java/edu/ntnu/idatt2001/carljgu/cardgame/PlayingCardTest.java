package edu.ntnu.idatt2001.carljgu.cardgame;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import edu.ntnu.idatt2001.carljgu.cardgame.cards.PlayingCard;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class PlayingCardTest {

  @Test
  public void card_has_to_be_one_of_the_four_eligible_suits() {
    new PlayingCard('S', 1);
    new PlayingCard('H', 1);
    new PlayingCard('D', 1);
    new PlayingCard('C', 1);

    try {
      new PlayingCard('A', 1);
      fail();
    } catch (IllegalArgumentException exception) {
      assertEquals("Suit is invalid", exception.getMessage());
    }
  }

  @Test
  public void card_face_can_not_be_negative_or_more_than_thirteen() {
    try {
      new PlayingCard('S', -1);
      fail();
    } catch (IllegalArgumentException exception) {
      assertEquals("Face is invalid", exception.getMessage());
    }

    try {
      new PlayingCard('S', 14);
      fail();
    } catch (IllegalArgumentException exception) {
      assertEquals("Face is invalid", exception.getMessage());
    }
  }
}
